﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using MathParserTK;

namespace codeChill
{
    public partial class Form1 : Form
    {
        int timerCount_1;
        int timerCount_2;
        List<KeyValuePair<double, double>> results;
        public Form1()
        {
            InitializeComponent();
            tableLayoutPanel1.Controls.Add(getNewCell("X"), 0, 0);
            tableLayoutPanel1.Controls.Add(getNewCell("Y"), 0, 1);
            timerCount_1 = 0;
            timerCount_2 = 0;
            chart1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            text_y.Width = 125;
            double minValue = Convert.ToDouble(minValueText.Text.Replace(".", ","));
            double maxValue = Convert.ToDouble(maxValueText.Text.Replace(".", ","));
            double interval = Convert.ToDouble(intervalText.Text.Replace(".", ","));
            results = new List<KeyValuePair<double, double>>();
            for (double d = minValue; d <= maxValue; d += interval)
            {
                try
                {
                    results.Add(new KeyValuePair<double, double>(d, myFunction(d)));
                }
                catch (Exception) { }
            }
            tableLayoutPanel1.Visible = true;
            button1.Visible = false;
            button2.Visible = true;
            timer1.Start();
            drawGraphics();
        }

        private Label getNewCell(String text)
        {
            Label cell = new Label();
            cell.Text = text;
            cell.TextAlign = ContentAlignment.MiddleCenter;
            return cell;
        }

        private void addNewCol(TableLayoutPanel tableLayoutPanel)
        {
            tableLayoutPanel.ColumnCount++;
            tableLayoutPanel.ColumnStyles[tableLayoutPanel.ColumnStyles.Count - 1].Width = 50;
            var colStyleNew = new ColumnStyle();
            colStyleNew.SizeType = SizeType.Absolute;
            tableLayoutPanel.ColumnStyles.Add(colStyleNew);
        }

        private double myFunction(double d)
        {
            MathParser parser = new MathParser();
            string x = d.ToString();
            string y = text_y.Text.Replace("x", x).Replace(".", ",");
            double result = parser.Parse(y, true);
            return result;
        }

        private void doubleValueValidator(object sender, KeyPressEventArgs e)
        {
            TextBox box = (TextBox)sender;
            bool withDot = box.Text.Contains('.');
            bool withMinus = box.Text.Contains('-');
            bool empty = box.Text.Length == 0;
            if ((!char.IsControl(e.KeyChar)
                && !char.IsDigit(e.KeyChar)
                && e.KeyChar != '.' && e.KeyChar != '-')
                || (e.KeyChar == '.' && (withDot || empty))
                || (e.KeyChar == '-' && (withMinus || !empty)))
            {
                e.Handled = true;
            }
        }

        private void text_y_TextChanged(object sender, EventArgs e)
        {
            TextBox box = (TextBox)sender;
            if (box.Text.Length > 18)
            {
                Size size = TextRenderer.MeasureText(box.Text, box.Font);
                box.Width = size.Width + 5;
                box.Height = size.Height;
            }
            else
            {
                box.Width = 125;
            }

            if (box.Width < 125)
                box.Width = 125;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (timerCount_1 >= results.Count || timerCount_1 >= 11)
            {
                tableLayoutPanel1.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
                if (timerCount_1 == 11)
                {
                    tableLayoutPanel1.ColumnCount--;
                    tableLayoutPanel2.Controls.Add(getNewCell("X"), 0, 0);
                    tableLayoutPanel2.Controls.Add(getNewCell("Y"), 0, 1);
                    tableLayoutPanel2.Visible = true;
                    timer2.Start();
                } 
                timer1.Stop();
            }
            else
            {
                addNewCol(tableLayoutPanel1);
                tableLayoutPanel1.Controls.Add(
                    getNewCell(Math.Round(results[timerCount_1].Key, 2).ToString()), timerCount_1 + 1, 0);
                tableLayoutPanel1.Controls.Add(
                    getNewCell(Math.Round(results[timerCount_1].Value, 2).ToString()), timerCount_1 + 1, 1);
                timerCount_1++;
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (timerCount_2 >= results.Count || timerCount_2 >= 11)
            {
                tableLayoutPanel2.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
                if (timerCount_2 == 11)
                {
                    tableLayoutPanel2.ColumnCount--;
                }
                timer2.Stop();
            }
            else
            {
                try
                {
                    addNewCol(tableLayoutPanel2);
                    tableLayoutPanel2.Controls.Add(
                        getNewCell(Math.Round(results[timerCount_2 + 10].Key, 2).ToString()), timerCount_2 + 1, 0);
                    tableLayoutPanel2.Controls.Add(
                        getNewCell(Math.Round(results[timerCount_2 + 10].Value, 2).ToString()), timerCount_2 + 1, 1);
                }
                catch (Exception) { }
                timerCount_2++;
            }
        }

        private void drawGraphics()
        {
            for (int i = 0; i < results.Count; i++)
            {
                chart1.Series[0].Points.AddXY(results[i].Key, results[i].Value);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
    }
}
