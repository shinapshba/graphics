﻿namespace codeChill
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.button1 = new System.Windows.Forms.Button();
            this.Point_HL = new System.Windows.Forms.Label();
            this.Point_HR = new System.Windows.Forms.Label();
            this.Point_LL = new System.Windows.Forms.Label();
            this.Point_LR = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.text_y = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.minValueText = new System.Windows.Forms.TextBox();
            this.maxValueText = new System.Windows.Forms.TextBox();
            this.intervalText = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.button2 = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(15, 429);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Point_HL
            // 
            this.Point_HL.AutoSize = true;
            this.Point_HL.Location = new System.Drawing.Point(411, 243);
            this.Point_HL.Name = "Point_HL";
            this.Point_HL.Size = new System.Drawing.Size(51, 13);
            this.Point_HL.TabIndex = 1;
            this.Point_HL.Text = "Point_HL";
            this.Point_HL.Visible = false;
            // 
            // Point_HR
            // 
            this.Point_HR.AutoSize = true;
            this.Point_HR.Location = new System.Drawing.Point(718, 243);
            this.Point_HR.Name = "Point_HR";
            this.Point_HR.Size = new System.Drawing.Size(53, 13);
            this.Point_HR.TabIndex = 2;
            this.Point_HR.Text = "Point_HR";
            this.Point_HR.Visible = false;
            // 
            // Point_LL
            // 
            this.Point_LL.AutoSize = true;
            this.Point_LL.Location = new System.Drawing.Point(411, 410);
            this.Point_LL.Name = "Point_LL";
            this.Point_LL.Size = new System.Drawing.Size(49, 13);
            this.Point_LL.TabIndex = 3;
            this.Point_LL.Text = "Point_LL";
            this.Point_LL.Visible = false;
            // 
            // Point_LR
            // 
            this.Point_LR.AutoSize = true;
            this.Point_LR.Location = new System.Drawing.Point(718, 410);
            this.Point_LR.Name = "Point_LR";
            this.Point_LR.Size = new System.Drawing.Size(51, 13);
            this.Point_LR.TabIndex = 4;
            this.Point_LR.Text = "Point_LR";
            this.Point_LR.Visible = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 716F));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(190, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(598, 45);
            this.tableLayoutPanel1.TabIndex = 5;
            this.tableLayoutPanel1.Visible = false;
            // 
            // text_y
            // 
            this.text_y.Location = new System.Drawing.Point(47, 12);
            this.text_y.Name = "text_y";
            this.text_y.Size = new System.Drawing.Size(125, 20);
            this.text_y.TabIndex = 6;
            this.text_y.TextChanged += new System.EventHandler(this.text_y_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "F(x) ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Interval";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Min Value";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Max Value";
            // 
            // minValueText
            // 
            this.minValueText.Location = new System.Drawing.Point(72, 41);
            this.minValueText.Name = "minValueText";
            this.minValueText.Size = new System.Drawing.Size(100, 20);
            this.minValueText.TabIndex = 14;
            this.minValueText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.doubleValueValidator);
            // 
            // maxValueText
            // 
            this.maxValueText.Location = new System.Drawing.Point(72, 67);
            this.maxValueText.Name = "maxValueText";
            this.maxValueText.Size = new System.Drawing.Size(100, 20);
            this.maxValueText.TabIndex = 15;
            this.maxValueText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.doubleValueValidator);
            // 
            // intervalText
            // 
            this.intervalText.Location = new System.Drawing.Point(72, 93);
            this.intervalText.Name = "intervalText";
            this.intervalText.Size = new System.Drawing.Size(100, 20);
            this.intervalText.TabIndex = 16;
            this.intervalText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.doubleValueValidator);
            // 
            // timer1
            // 
            this.timer1.Interval = 50;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // chart1
            // 
            this.chart1.BorderlineColor = System.Drawing.Color.Gray;
            this.chart1.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartArea2.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart1.Legends.Add(legend2);
            this.chart1.Location = new System.Drawing.Point(15, 123);
            this.chart1.Name = "chart1";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chart1.Series.Add(series2);
            this.chart1.Size = new System.Drawing.Size(773, 300);
            this.chart1.TabIndex = 17;
            this.chart1.Text = "chart1";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(15, 429);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 18;
            this.button2.Text = "Restart";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 716F));
            this.tableLayoutPanel2.Location = new System.Drawing.Point(190, 64);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(598, 45);
            this.tableLayoutPanel2.TabIndex = 6;
            this.tableLayoutPanel2.Visible = false;
            // 
            // timer2
            // 
            this.timer2.Interval = 50;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 466);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.intervalText);
            this.Controls.Add(this.maxValueText);
            this.Controls.Add(this.minValueText);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.text_y);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.Point_LR);
            this.Controls.Add(this.Point_LL);
            this.Controls.Add(this.Point_HR);
            this.Controls.Add(this.Point_HL);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label Point_HL;
        private System.Windows.Forms.Label Point_HR;
        private System.Windows.Forms.Label Point_LL;
        private System.Windows.Forms.Label Point_LR;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox text_y;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox minValueText;
        private System.Windows.Forms.TextBox maxValueText;
        private System.Windows.Forms.TextBox intervalText;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Timer timer2;
    }
}

